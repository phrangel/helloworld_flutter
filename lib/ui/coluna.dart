import 'package:flutter/material.dart';

class Coluna extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Text(
            "TESTE 1",
            textDirection: TextDirection.ltr,
            style: TextStyle(
              fontStyle: FontStyle.italic,
            ),
          ),
          Text(
            "TESTE 2",
            textDirection: TextDirection.ltr,
            style: TextStyle(
              fontStyle: FontStyle.italic,
            ),
          ),
          FlatButton(
            onPressed: ()=> "Hello",
            child: Text("Botão"),
            color: Colors.blueAccent,
          )
        ],
      ),
    );
  }
}
