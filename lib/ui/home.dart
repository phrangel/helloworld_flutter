import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Andaime"),
        backgroundColor: Colors.blueAccent,
        actions: <Widget>[
          IconButton(
            icon: new Icon(Icons.add),
            onPressed: () => debugPrint("Teste"),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Teste",
              style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.w800,
                  color: Colors.blueAccent),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.access_alarm), title: Text("Perfil")),
        BottomNavigationBarItem(
            icon: Icon(Icons.access_alarm), title: Text("Perfil")),
        BottomNavigationBarItem(
            icon: Icon(Icons.access_alarm), title: Text("Perfil")),
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () => debugPrint("TESTE"),
        backgroundColor: Colors.blueAccent,
        child: Icon(Icons.add),
      ),
    );
  }
}
